﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nancy.Configuration;
using Nancy.TinyIoc;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Bloom.Middleware
{
    public class NetCoreBootstrapper : DefaultNancyBootstrapper
    {
        readonly IServiceCollection _services;
        
        public NetCoreBootstrapper(IServiceCollection services)
        {
            _services = services;
        }

        public override void Configure(INancyEnvironment environment)
        {
            environment.Tracing(true, true);
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);
            
            foreach (var service in _services)
            {
                container.Register(service);
            }
            
            
        }
    }
}
