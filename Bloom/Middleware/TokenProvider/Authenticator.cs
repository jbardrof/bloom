﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Bloom.Middleware.TokenProvider
{
    public interface IAuthenticator
    {
        Task<ClaimsIdentity> AuthenticateIdentity(string username, string password);
    }
    public class Authenticator : IAuthenticator
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public Authenticator(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public Task<ClaimsIdentity> AuthenticateIdentity(string username, string password)
        {
            var user = _userManager.FindByEmailAsync(username).Result;

            if (user == null || !user.EmailConfirmed)
            {
                return null;
            }

            var passwordSignInResult = _signInManager.PasswordSignInAsync(user,
                password,
                isPersistent: false,
                lockoutOnFailure: false).Result;

            if (!passwordSignInResult.Succeeded)
            {
                return null;
            }
            else
            {
                var claims = _userManager.GetClaimsAsync(user).Result;

                return Task.FromResult(new ClaimsIdentity(new GenericIdentity(user.Email, "Token"), claims));
            }
        }
        }
}
