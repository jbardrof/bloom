﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Models
{
    public class Persona
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [StringLength(450)]
        public string IdentityId { get; set; }

        public ICollection<PersonaProficiencyLevel> ProficiencyLevels { get; set; }
    }
}
