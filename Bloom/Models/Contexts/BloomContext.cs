﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Models
{
    public class BloomContext : DbContext
    {
        public DbSet<Persona> Personas { get; set; }

        public DbSet<Track> Tracks { get; set; }

        public BloomContext(DbContextOptions<BloomContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<PersonaProficiencyLevel>()
                .HasKey(p => new { p.PersonaId, p.ProficiencyLevelId });
        }
    }
}
