﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Models
{
    public class PersonaProficiencyLevel
    {
        public int PersonaId { get; set; }
        
        public int ProficiencyLevelId { get; set; }

        [ForeignKey("PersonaId")]
        public Persona Persona { get; set; }

        [ForeignKey("ProficiencyLevelId")]
        public ProficiencyLevel ProficiencyLevel { get; set; }
    }
}
