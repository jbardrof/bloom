﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Models
{
    public class Track
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ProficiencyCategory> Categories { get; set; }
    }
}
