﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Models
{
    public class ProficiencyLevel
    {
        public int Id { get; set; }
        [Required]
        public int Value { get; set; }
        public string Description { get; set; }


        public int ProficiencyId { get; set; }
        [ForeignKey("ProficiencyId")]
        public Proficiency Proficiency { get; set; }

        public ICollection<Persona> Personas { get; set; }
    }
}
