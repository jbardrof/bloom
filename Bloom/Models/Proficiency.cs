﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Models
{
    public class Proficiency
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<ProficiencyLevel> Levels { get; set; }

        public int ProficiencyCategoryId { get; set; }
        [ForeignKey("ProficiencyCategoryId")]
        public ProficiencyCategory Category { get; set; }
    }
}
