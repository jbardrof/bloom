﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Repositories
{
    public interface IIdentityRepository
    {
        Task<IdentityResult> CreateAsync(IdentityUser identity, string password);
        Task<IdentityResult> ConfirmEmailAsync(IdentityUser identity, string token);
        Task<string> GenerateEmailConfirmationTokenAsync(IdentityUser identity);
        Task<IdentityUser> FindByIdAsync(string id);
    }

    public class IdentityRepository : IIdentityRepository
    {
        private readonly UserManager<IdentityUser> _userManager;
        public IdentityRepository(UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
        }

        public Task<IdentityResult> ConfirmEmailAsync(IdentityUser identity, string token)
        {
            return _userManager.ConfirmEmailAsync(identity, token);
        }

        public Task<IdentityResult> CreateAsync(IdentityUser identity, string password)
        {
            return _userManager.CreateAsync(identity, password);
        }

        public Task<IdentityUser> FindByIdAsync(string id)
        {
            return _userManager.FindByIdAsync(id);
        }

        public Task<string> GenerateEmailConfirmationTokenAsync(IdentityUser identity)
        {
            return _userManager.GenerateEmailConfirmationTokenAsync(identity);
        }
    }
}
