﻿var path = require('path');

module.exports = {
    entry: {
        main: './Scripts/project'
    },
    output: {
        publicPath: '/js',
        path: path.join(__dirname, '/wwwroot/js'),
        filename: 'project.build.js'
    }
};