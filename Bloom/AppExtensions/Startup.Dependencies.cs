﻿using Bloom.Middleware.TokenProvider;
using Bloom.Resources.Authentication;
using Bloom.Resources.Messaging;
using Bloom.Resources.Registrations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace Bloom.AppExtensions
{
    public static class DependenciesExtensions
    {
        public static IServiceCollection ConfigureDependencies(this IServiceCollection services)
        {
            // Register Services into the Container
            services.AddTransient<IAuthenticator, Authenticator>();
            services.AddTransient<IMessageService, FileMessageService>();
            services.AddTransient<IRegistrationService, RegistrationService>();
            services.AddTransient<IAuthenticationService, AuthenticationService>();

            // Allows the Url Helper to be used outside of the controllers.
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IUrlHelper>(h =>
            {
                var actionContext = h.GetService<IActionContextAccessor>().ActionContext;
                return new UrlHelper(actionContext);
            });

            return services;
        }
    }
}
