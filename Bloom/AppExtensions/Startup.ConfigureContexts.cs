﻿using Bloom.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Bloom.AppExtensions
{
    public static class ContextExtensions
    {
        public static IServiceCollection ConfigureContexts(this IServiceCollection services, IConfigurationRoot config)
        {
            // Set up the connection to the Identity Store
            services.AddDbContext<IdentityContext>(options =>
                options.UseSqlServer(config.GetConnectionString("IdentityConnectionString")));

            services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

            // Set up the connection to the main system database
            services.AddDbContext<BloomContext>(options =>
                options.UseSqlServer(config.GetConnectionString("SystemConnectionString")));

            return services;
        }
    }
}
