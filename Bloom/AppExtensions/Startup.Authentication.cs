﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Bloom.AppExtensions
{
    public static class AuthenticationExtensions
    { 
        public static IServiceCollection ConfigureAuthentication(this IServiceCollection services)
        {
            var signingCredentials = new SigningCredentials(
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secrets.SecretKey)), SecurityAlgorithms.HmacSha256);

            services.AddAuthentication(opt =>
            {
                opt.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        IssuerSigningKey = signingCredentials.Key,

                        ValidIssuer = "http://localhost:58323",

                        ValidAudience = "http://localhost:58323",

                        // Validate the token expiry
                        //ValidateLifetime = true,

                        // If you want to allow a certain amount of clock drift, set that here:
                        //ClockSkew = TimeSpan.Zero
                    };
                });
            return services;
        }
    }
}
