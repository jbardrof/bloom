﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Controllers
{

    [Route("/api/[controller]")]
    public class ProfilesController : ProtectedController
    {
        public ProfilesController(IServiceProvider serviceProvider) : base(serviceProvider) { }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var authenticateInfo = await HttpContext.GetTokenAsync("access_token");
            return Ok(new { value = "123" });
        }
    }
}
