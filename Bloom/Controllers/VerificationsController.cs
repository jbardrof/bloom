﻿using Bloom.Resources.Registrations;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Controllers
{
    [Route("api/[controller]")]
    public class VerificationsController : Controller
    {
        private readonly IRegistrationService _registrationService;
        public VerificationsController(IRegistrationService registrationService)
        {
            _registrationService = registrationService;
        }

        /// <summary>
        /// Action to start the verification process
        /// </summary>
        /// <param name="id">Registration Id</param>
        /// <param name="token">Registration token</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id, string token)
        {
            var registration = await _registrationService.VerifyRegistration(id, token);

            if (registration == null)
            {
                return NotFound();
            }
            else if (registration?.Errors?.Count() > 0)
            {
                return BadRequest(registration.Errors);
            }

            return Ok();
        }
    }
}
