﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Bloom.Controllers
{

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ProtectedController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        protected IdentityUser CurrentUser;

        public ProtectedController(IServiceProvider serviceProvider)
        {
            _userManager = (UserManager<IdentityUser>)serviceProvider.GetService(
                typeof(UserManager<IdentityUser>));
        }

        public override void OnActionExecuting(ActionExecutingContext ctx)
        {
            base.OnActionExecuting(ctx);

            // Determine if the user is authenticated by their token
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                // From the claims collection, find their identity id.
                var identityId = HttpContext.User.Claims.SingleOrDefault(
                    x => x.Type == ClaimTypes.NameIdentifier).Value;

                // If for some reason we cannot find the identity even though
                // the user is authenticated, return a 401. Something is wrong.
                if (identityId == null)
                {
                    // TODO: log these results, something is funky if we get here.
                    ctx.Result = new UnauthorizedResult();
                }

                CurrentUser = _userManager.FindByIdAsync(identityId).Result;

            }
        }
    }
}
