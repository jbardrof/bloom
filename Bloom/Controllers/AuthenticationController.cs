﻿using Bloom.Resources.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Post(Authentication auth)
        {
            var authResult = await _authenticationService.Authenticate(auth);

            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(authResult.AuthToken),
                expiration = authResult.AuthToken.ValidTo
            });
        }
    }
}
