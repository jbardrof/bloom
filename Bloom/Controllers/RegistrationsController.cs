﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.JsonPatch;
using Bloom.Resources.Messaging;
using Bloom.Resources.Registrations;

namespace Bloom.Controllers
{
    [Route("api/[controller]")]
    public class RegistrationsController : Controller
    {
        private readonly IRegistrationService _registrationService;

        public RegistrationsController(IRegistrationService registrationService)
        {
            _registrationService = registrationService;
        }


        // GET: api/identity
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/identity/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/identity
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Registration registration)
        {
            var registrationResult = await _registrationService.Register(registration);

            if( registrationResult?.Errors?.Count() > 0)
            {
                return BadRequest(registrationResult.Errors);
            }
            else
            {
                return Ok(registrationResult);
            }
            
        }

        [HttpPatch("{id}")]
        public void Patch(string id, JsonPatchDocument<Registration> registration)
        {
        }

        // PUT api/identity/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/identity/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        /*[HttpPatch]
        public async Task<IActionResult> Patch([FromBody]JsonPatchDocument<IdentityUser> patchObject)
        {
            return Ok();
        }*/
    }
}
