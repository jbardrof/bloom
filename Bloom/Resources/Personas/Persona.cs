﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Personas
{
    public class Persona
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<PersonaProficiency> Proficiencies { get; set; }
    }
}
