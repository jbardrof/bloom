﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Personas
{
    public class PersonaProficiency
    {
        public int Id { get; set; }
        public int LevelId { get; set; }
        public string ProficiencyName { get; set; }
        public string LevelDescription { get; set; }
        public int LevelValue { get; set; }

    }
}
