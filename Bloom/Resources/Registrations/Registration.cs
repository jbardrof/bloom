﻿using FluentValidation;
using FluentValidation.Results;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Registrations
{
    public class Registration : BloomResource
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }

        public RegistrationState State { get; set; }
        

        public ValidationResult Validate()
        {
            var validator = new RegistrationValidator();
            return validator.Validate(this);
        }

        // Follows the JSON.NET standard for not serializing a property.
        // JsonIgnore will simply never serialize or deserialize a property
        // we want to deserialize from requests, but we never want to send
        // a password back over the wire. These methods insure the password
        // never gets sent back out.
        public bool ShouldSerializePassword() { return false; }
        public bool ShouldSerializePasswordConfirmation() { return false; }
    }

    public class RegistrationValidator : AbstractValidator<Registration>
    {
        public RegistrationValidator()
        {
            RuleFor(r => r.Email)
                .NotEmpty();

            RuleFor(r => r.Name)
                .NotEmpty()
                .MinimumLength(5)
                .MaximumLength(100);

            RuleFor(r => r.Password)
                .NotEmpty()
                .MinimumLength(8)
                .Equal(r => r.PasswordConfirmation);
        }
    }

    public enum RegistrationState
    {
        Pending, Verified, Cancelled
    }
}
