﻿using Bloom.Models;
using Bloom.Repositories;
using Bloom.Resources.Messaging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Registrations
{
    public class RegistrationService : IRegistrationService
    {
        private readonly IIdentityRepository _identityRepo;
        private readonly IMessageService _messageService;
        private readonly BloomContext _db;
        private readonly MessageOptions _msgOptions;

        public RegistrationService(IIdentityRepository identityRepository, 
            IMessageService messageService, 
            BloomContext db, 
            IOptions<MessageOptions> msgOptions)
        {
            _identityRepo = identityRepository;
            _messageService = messageService;
            _db = db;
            _msgOptions = msgOptions.Value;
        }

        public Registration Find(int id)
        {
            return null;
        }

        public async Task<TaskResult<Registration>> Register(Registration registration)
        {
            var result = new TaskResult<Registration> { Target = registration };

            var validationResults = registration.Validate();
            if (!validationResults.IsValid)
            {
                result.Errors = validationResults.Errors;
                return result;
            }            

            var newIdentity = new IdentityUser
            {
                UserName = registration.Email,
                Email = registration.Email
            };

            var userCreationResult = await _identityRepo.CreateAsync(newIdentity, 
                registration.Password);

            if (!userCreationResult.Succeeded)
            {
                result.Errors = Error.ConvertToValidationFailure(userCreationResult.Errors);

                return result;
            }

            var persona = await _db.Personas.AddAsync(new Persona
            {
                Name = registration.Name,
                IdentityId = newIdentity.Id
            });

            var x = await _db.SaveChangesAsync();

            result.Target.Id = newIdentity.Id;

            SendRegistrationConfirmation(newIdentity);

            return result;
        }

        public async Task<TaskResult<Registration>> VerifyRegistration(string id, string token)
        {
            var result = new TaskResult<Registration>();
            var identity = await _identityRepo.FindByIdAsync(id);

            if (identity == null)
            {
                return null;
            }

            var persona = _db.Personas.Where(p => p.IdentityId == identity.Id).First();

            result.Target = new Registration
            {
                Name = persona.Name,
                Email = identity.Email,
                Id = identity.Id
            };

            var verificationResult = await _identityRepo.ConfirmEmailAsync(identity, token);

            if (verificationResult.Succeeded)
            {
                result.Target.State = RegistrationState.Verified;
            }
            else
            {
                result.Errors = Error.ConvertToValidationFailure(verificationResult.Errors);
            }

            return result;
        }

        private void SendRegistrationConfirmation(IdentityUser newIdentity)
        {
            var emailConfirmationToken = _identityRepo
                .GenerateEmailConfirmationTokenAsync(newIdentity).Result;

            var tokenVerificationUrl = String.Format(_msgOptions.RegistrationConfirmationUrl,
                newIdentity.Id, emailConfirmationToken);

            var sender = _messageService.Send(newIdentity.Email, "Verify your email", 
                $"Click <a href=\"{tokenVerificationUrl}\">here</a> to verify your email");
        }


    }
}
