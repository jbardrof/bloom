﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Registrations
{
    public interface IRegistrationService
    {
        Task<TaskResult<Registration>> Register(Registration registration);
        Task<TaskResult<Registration>> VerifyRegistration(string id, string token);
        Registration Find(int id);
    }
}
