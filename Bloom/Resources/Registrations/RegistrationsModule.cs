﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Nancy;
using Nancy.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Registrations
{
    public class RegistrationsModule : NancyModule
    {
        public RegistrationsModule(IRegistrationService registrationsService)
        {
            Get("/registrations/", args =>
            {
                return Task.FromResult(200);
            });

            Get("/registrations/{id}", (args, ct) => {
                return Task.FromResult(registrationsService.Find(args.id));
            });

            Post("/registrations/", (args, ct) =>
            {
                return Task.FromResult(registrationsService.Register(this.Bind<Registration>()));
            });
        }
    }
}
