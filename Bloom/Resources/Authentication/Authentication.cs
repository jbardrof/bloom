﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Authentication
{
    public class Authentication
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; } = false;
        public SecurityToken AuthToken { get; set; }

        public bool ShouldSerializePassword() { return false; }
    }
}
