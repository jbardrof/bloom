﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Authentication
{
    public interface IAuthenticationService
    {
        Task<Authentication> Authenticate(Authentication auth);
    }
}
