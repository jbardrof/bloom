﻿using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Bloom.Resources.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly SignInManager<IdentityUser> _signInManager;

        public AuthenticationService(SignInManager<IdentityUser> signInManager)
        {
            _signInManager = signInManager;
        }

        public async Task<Authentication> Authenticate(Authentication auth)
        {
            var authResult = await _signInManager.PasswordSignInAsync(auth.Email, auth.Password,
                auth.Remember, lockoutOnFailure: false);

            if (authResult.Succeeded)
            {
                var user = await _signInManager.UserManager.FindByEmailAsync(auth.Email);
                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secrets.SecretKey));
                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken("http://localhost:58323",
                    "http://localhost:58323",
                    claims: CreateUserClaims(user),
                    expires: DateTime.Now.AddMinutes(60),
                    signingCredentials: credentials);

                auth.AuthToken = token;

                return auth;
            }

            return null;
        }
        

        private IEnumerable<Claim> CreateUserClaims(IdentityUser user)
        {
            return new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, user.Email)
            };
        }
    }
}
