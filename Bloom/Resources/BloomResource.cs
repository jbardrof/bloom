﻿using System;

namespace Bloom.Resources
{
    public abstract class BloomResource
    {
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public BloomResource()
        {
            CreatedAt = (CreatedAt == DateTime.MinValue) ? DateTime.Now.ToUniversalTime() : CreatedAt;
            UpdatedAt = (UpdatedAt == DateTime.MinValue) ? DateTime.Now.ToUniversalTime() : UpdatedAt;
        }
    }
}
