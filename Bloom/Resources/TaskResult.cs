﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources
{
    public class TaskResult<T> where T : class  
    {
        public IEnumerable<ValidationFailure> Errors { get; set; }
        public T Target { get; set; }
    }

    public class Error
    {
        //
        // Summary:
        //     Gets or sets the code for this error.
        public string Code { get; set; }
        //
        // Summary:
        //     Gets or sets the description for this error.
        public string Description { get; set; }

        public static IEnumerable<ValidationFailure> ConvertToValidationFailure(
            IEnumerable<IdentityError> errors)
        {
            var result = new List<ValidationFailure>();
            foreach (var error in errors)
            {
                result.Add(new ValidationFailure(error.Code, error.Description));
            }

            return result;
        }
    }
}
