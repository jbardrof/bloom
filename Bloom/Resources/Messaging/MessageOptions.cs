﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bloom.Resources.Messaging
{
    public class MessageOptions
    {
        public string RegistrationConfirmationUrl { get; set; }
    }
}
