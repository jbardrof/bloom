﻿using System.Threading.Tasks;

namespace Bloom.Resources.Messaging
{
    public interface IMessageService
    {
        Task Send(string email, string subject, string message);
    }
}