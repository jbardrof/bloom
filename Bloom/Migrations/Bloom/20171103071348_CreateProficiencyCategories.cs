﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Bloom.Migrations.Bloom
{
    public partial class CreateProficiencyCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Proficiency_Tracks_TrackId",
                table: "Proficiency");

            migrationBuilder.DropIndex(
                name: "IX_Proficiency_TrackId",
                table: "Proficiency");

            migrationBuilder.DropColumn(
                name: "TrackId",
                table: "Proficiency");

            migrationBuilder.AddColumn<int>(
                name: "ProficiencyCategoryId",
                table: "Proficiency",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ProficiencyLevelId",
                table: "Personas",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PersonaProficiencyLevel",
                columns: table => new
                {
                    PersonaId = table.Column<int>(type: "int", nullable: false),
                    ProficiencyLevelId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PersonaProficiencyLevel", x => new { x.PersonaId, x.ProficiencyLevelId });
                    table.ForeignKey(
                        name: "FK_PersonaProficiencyLevel_Personas_PersonaId",
                        column: x => x.PersonaId,
                        principalTable: "Personas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PersonaProficiencyLevel_ProficiencyLevel_ProficiencyLevelId",
                        column: x => x.ProficiencyLevelId,
                        principalTable: "ProficiencyLevel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProficiencyCategory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TrackId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProficiencyCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProficiencyCategory_Tracks_TrackId",
                        column: x => x.TrackId,
                        principalTable: "Tracks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Proficiency_ProficiencyCategoryId",
                table: "Proficiency",
                column: "ProficiencyCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Personas_ProficiencyLevelId",
                table: "Personas",
                column: "ProficiencyLevelId");

            migrationBuilder.CreateIndex(
                name: "IX_PersonaProficiencyLevel_ProficiencyLevelId",
                table: "PersonaProficiencyLevel",
                column: "ProficiencyLevelId");

            migrationBuilder.CreateIndex(
                name: "IX_ProficiencyCategory_TrackId",
                table: "ProficiencyCategory",
                column: "TrackId");

            migrationBuilder.AddForeignKey(
                name: "FK_Personas_ProficiencyLevel_ProficiencyLevelId",
                table: "Personas",
                column: "ProficiencyLevelId",
                principalTable: "ProficiencyLevel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Proficiency_ProficiencyCategory_ProficiencyCategoryId",
                table: "Proficiency",
                column: "ProficiencyCategoryId",
                principalTable: "ProficiencyCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Personas_ProficiencyLevel_ProficiencyLevelId",
                table: "Personas");

            migrationBuilder.DropForeignKey(
                name: "FK_Proficiency_ProficiencyCategory_ProficiencyCategoryId",
                table: "Proficiency");

            migrationBuilder.DropTable(
                name: "PersonaProficiencyLevel");

            migrationBuilder.DropTable(
                name: "ProficiencyCategory");

            migrationBuilder.DropIndex(
                name: "IX_Proficiency_ProficiencyCategoryId",
                table: "Proficiency");

            migrationBuilder.DropIndex(
                name: "IX_Personas_ProficiencyLevelId",
                table: "Personas");

            migrationBuilder.DropColumn(
                name: "ProficiencyCategoryId",
                table: "Proficiency");

            migrationBuilder.DropColumn(
                name: "ProficiencyLevelId",
                table: "Personas");

            migrationBuilder.AddColumn<int>(
                name: "TrackId",
                table: "Proficiency",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Proficiency_TrackId",
                table: "Proficiency",
                column: "TrackId");

            migrationBuilder.AddForeignKey(
                name: "FK_Proficiency_Tracks_TrackId",
                table: "Proficiency",
                column: "TrackId",
                principalTable: "Tracks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
