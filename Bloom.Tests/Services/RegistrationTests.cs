﻿using Bloom.Models;
using Bloom.Repositories;
using Bloom.Resources.Messaging;
using Bloom.Resources.Registrations;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Bloom.Tests.Services
{
    public class RegistrationTests
    {
        [Fact]
        public void TestRegistration()
        {
            var messageService = new Mock<IMessageService>();

            var identityRepo = new Mock<IIdentityRepository>();
            var dbContext = new BloomContext(OptionsBuilder());
            var msgOptions = new Mock<IOptions<MessageOptions>>();
            var userManager = BuildUserManager();


            identityRepo.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);
            identityRepo.Setup(x => x.GenerateEmailConfirmationTokenAsync(It.IsAny<IdentityUser>()))
                .ReturnsAsync("token");
            messageService.Setup(x => x.Send("", "", "")).Returns(It.IsAny<Task>());
            msgOptions.Setup(x => x.Value).Returns(new MessageOptions {
                RegistrationConfirmationUrl = "http://localhost:58323/api/verifications/{0}?token={1}"
            });
            

            var registrationService = new RegistrationService(identityRepo.Object, 
                messageService.Object, dbContext, msgOptions.Object);

            var result = registrationService.Register(new Registration
            {
                Name = "Robert",
                Email = "robert@bloom.io",
                Password = "Test!1234",
                PasswordConfirmation = "Test!1234"
            }).Result;

            Assert.Equal("robert@bloom.io", result.Target.Email);
        } 

        private DbContextOptions<BloomContext> OptionsBuilder()
        {
            return new DbContextOptionsBuilder<BloomContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
        }

        private UserManager<IdentityUser> BuildUserManager()
        {
            var userStore = new Mock<IUserStore<IdentityUser>>().Object;
            var userOptions = new Mock<IOptions<IdentityOptions>>().Object;
            var passwordHasher = new Mock<IPasswordHasher<IdentityUser>>().Object;
            var userValidators = new IUserValidator<IdentityUser>[0];
            var passwordValidators = new IPasswordValidator<IdentityUser>[0]; 
            var lookupNormalizer = new Mock<ILookupNormalizer>().Object;
            var identityErrorDescriber = new Mock<IdentityErrorDescriber>().Object;
            var serviceProvider = new Mock<IServiceProvider>().Object;
            var logger = new Mock<ILogger<UserManager<IdentityUser>>>().Object;

            var userManager = new UserManager<IdentityUser>(userStore, userOptions,
                passwordHasher, userValidators, passwordValidators, lookupNormalizer,
                identityErrorDescriber, serviceProvider, logger);

            return userManager;
        }
    }
}
